import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LoginService } from './../../service/login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  event=true;
  public productForm: FormGroup;

  constructor(private fb: FormBuilder,private LoginService:LoginService) {
    this.productForm = this.fb.group({
      businessName: ['', [Validators.required]],
      name: ['', [Validators.required]],
      email: ['', [Validators.required]],
      phoneNumber: ['', [Validators.required]],
      address: ['', [Validators.required]],
      accountId: ['790'],

    })
   }

  ngOnInit(): void {
  }
  register(){
    this.event=false;
  }
  login(){
    this.event=true;
  }
  submit(){     
    console.log(this.productForm.valid)

     if(this.productForm.valid){
      this.LoginService.register(this.productForm.value).subscribe((result) =>{
        this.productForm.reset();
      })
     }
  }
  redirect(){
    window.location.href = 'http://manager.clexpress.lk';
  }
}
