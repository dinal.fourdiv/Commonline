import { Component, OnInit } from '@angular/core';
import { faCheck, faCaretRight, faAngleRight, faBookReader, faChalkboardTeacher, faSchool, faAward} from '@fortawesome/free-solid-svg-icons';


@Component({
  selector: 'app-about-us',
  templateUrl: './about-us.component.html',
  styleUrls: ['./about-us.component.scss']
})
export class AboutUsComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }
  faCheck = faCheck;
  faCaretRight = faCaretRight;
  faAngleRight = faAngleRight;
  faBookReader = faBookReader;
  faChalkboardTeacher = faChalkboardTeacher;
  faSchool = faSchool;
  faAward = faAward;

  teachers= [
    {'id':1,'fname':'','lname':'SRI','subject':'','img':'../../assets/PARTNER/asian silk route logo.jpg'},
    {'id':2,'fname':'MICHELLE','lname':'ANNE','subject':'','img':'../../assets/PARTNER/jetgoo.jpg'},
    {'id':3,'fname':'MALIK','lname':'DOE','subject':'','img':'../../assets/PARTNER/SF EXPRESS INT LOGO.png'},
    {'id':4,'fname':'JAKSON','lname':'SILVA','subject':'','img':'../../assets/PARTNER/venco logo.png'}, 
    {'id':5,'fname':'JAKSON','lname':'SILVA','subject':'','img':'../../assets/PARTNER/Ali.png'},   
  
  ];
}
