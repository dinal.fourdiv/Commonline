import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { TrakingService } from 'src/app/service/traking.service';
import { find, get, pull } from 'lodash';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
@Component({
  selector: 'app-timeline',
  templateUrl: './timeline.component.html',
  styleUrls: ['./timeline.component.scss'],
})
export class TimelineComponent implements OnInit {
  ids = [];
  passcode: any;
  details: any;
  error:any;
  arry = [];
  @ViewChild('tagInput') tagInputRef!: ElementRef;
  tags: string[] = ['html', 'Angular'];
  form!: FormGroup;
  number = '';
  @ViewChild('content', { static: false }) private content: any;

  constructor(
    private trakingService: TrakingService,
    private fb: FormBuilder,
    private modalService: NgbModal
  ) {}

  ngOnInit(): void {
    this.ids.forEach((e) => {
      this.number = this.number + '' + e + ',';
    });

    this.number = this.number.slice(0, -1);
    this.trakingService.getDetails(this.number, this.passcode).subscribe(
      (data: any) => {
        this.details = data;
        this.arry = data.itemHistoryTracking;
        console.log(data);
      },
      (err) => {
        this.error = err;
        this.details = '';
        // this.modalService.dismissAll();
      }
    );
  }
}
