import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class EmailService {

  constructor(private httpClient: HttpClient) { }


  sendMail(body:any) {
    const httpHeaders = new HttpHeaders({
      "Content-Type": "application/json",
    });

    const options = {
      headers: httpHeaders,
    };
    let url = "https://72i8t1e1sb.execute-api.ap-south-1.amazonaws.com/emails";
    return this.httpClient.post(url,body,options);
  }
}
