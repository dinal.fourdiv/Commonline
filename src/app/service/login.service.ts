import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private httpClient: HttpClient) { }

  onLogin(loginInfo:any) {
    let url = "http://userservice.dreamworld.lk/auth/login";
    return this.httpClient.post(url, loginInfo);
  }
  register(registerInfo:any) {
    const httpHeaders = new HttpHeaders({
      "Content-Type": "application/json",
    });

    const options = {
      headers: httpHeaders,
    };
    let url = "https://72i8t1e1sb.execute-api.ap-south-1.amazonaws.com/sign-up-request";
    return this.httpClient.post(url,registerInfo,options);
  }
}
